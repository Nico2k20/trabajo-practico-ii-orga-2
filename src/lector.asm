section .bss

arrayNuevo resb 262144

section .text


global aclarar_mmx
global aclarar_assembly
global promedio_assembly
global multiplyBlend_assembly


aclarar_assembly:
        push    ebp
        mov     ebp, esp

        mov     DWORD  [ebp-8], 0   
for1A:
        cmp     DWORD  [ebp-8], 511 
        jg      salirA
        mov     DWORD  [ebp-12], 0  
for2A:        
        cmp     DWORD  [ebp-12], 511 
        jg      volverA
        ;red
        mov     eax, DWORD  [ebp-8]             
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+8]             
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]            
        add     eax, ecx
        
        mov     ebx,DWORD[ebp+20]              
        add     DWORD[eax],ebx

        ;green
        mov     eax, DWORD  [ebp-8] 
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+16] 
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]
        add     eax, ecx
        
        mov     ebx,DWORD[ebp+20]
        add     DWORD[eax],ebx

        ;blue
        mov     eax, DWORD  [ebp-8] 
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+12] 
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]
        add     eax, ecx

        mov     ebx,DWORD[ebp+20]
        add     DWORD[eax],ebx


               inc     DWORD  [ebp-12]
        jmp     for2A
volverA:
        inc     DWORD  [ebp-8]
        jmp     for1A
salirA:
        nop
        pop     ebp
        ret


multiplyBlend_assembly:
        push    ebp
        push    edi
        push    esi
        push    ebx
        sub     esp, 16
        mov     eax, DWORD  [esp+40]
        mov     ebp, DWORD  [esp+36]
        mov     edi, DWORD  [esp+48]
        mov     esi, DWORD  [esp+52]
        mov     DWORD  [esp+8], eax
        mov     eax, DWORD  [esp+44]
        mov     ebx, DWORD  [esp+56]
        mov     DWORD  [esp+4], eax
        lea     eax, [ebp+2048]
        mov     DWORD  [esp+12], eax
primerFor:
        mov     DWORD  [esp], ebx
        xor     edx, edx
segundoFor:
        mov     ecx, DWORD  [ebp+0]
        mov     ebx, DWORD  [edi]
        add     ecx, edx
        movzx   eax, BYTE  [ecx]
        mul     BYTE  [ebx+edx]
        mov     BYTE  [ecx], al
        mov     eax, DWORD  [esp+8]
        mov     ebx, DWORD  [esi]
        mov     ecx, DWORD  [eax]
        add     ecx, edx
        movzx   eax, BYTE  [ecx]
        mul     BYTE  [ebx+edx]
        mov     ebx, DWORD  [esp]
        mov     BYTE  [ecx], al
        mov     eax, DWORD [esp+4]
        mov     ebx, DWORD  [ebx]
        mov     ecx, DWORD  [eax]
        add     ecx, edx
        movzx   eax, BYTE  [ecx]
        mul     BYTE  [ebx+edx]
        add     edx, 1
        mov     BYTE  [ecx], al
        cmp     edx, 512
        jne     segundoFor
        mov     ebx, DWORD  [esp]
        add     ebp, 4
        add     edi, 4
        add     DWORD  [esp+8], 4
        add     esi, 4
        add     DWORD  [esp+4], 4
        add     ebx, 4
        cmp     DWORD  [esp+12], ebp
        jne     primerFor
        add     esp, 16
        pop     ebx
        pop     esi
        pop     edi
        pop     ebp
        ret




sacar_promedio_assembly:
 ;     mov edx , DWORD[EBP-8]


  ;    cicloP:

  ;    inc edx
  ;    cmp edx, DWORD[EBP-8]+DWORD[ebp+20]
  ;    jg for2
  ;    mov ecx, DWORD[EBP-12]
  ;    ciclo2p:
  ;    cmp ecx,DWORD[EBP-12]+DWORD[EBP+20]
  ;    lea ecx , [0+eax*4]
  ;    jg cicloP



promedio_assembly:
        push    ebp
        mov     ebp, esp
        mov     DWORD  [ebp-8], 0   ;i=0
        mov     ebx,DWORD[ebp+20]
        mov     eax,511
        sub eax,ebx
for1P:
        cmp     DWORD  [ebp-8], eax ;i<511
        jg      salirP
        mov     DWORD  [ebp-12], 0  ;j=0
for2P:        
        cmp     DWORD  [ebp-12], eax ;j<511
        jg      volverP
        ;red
        mov     eax, DWORD  [ebp-8]             ;i
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+8]             ;matriz
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]            ;j
        add     eax, ecx
        
        jmp sacar_promedio_assembly
   ;    mov eax , [resultadoP]
        ;green
        mov     eax, DWORD  [ebp-8] 
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+16] 
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]
        add     eax, ecx
        
        jmp sacar_promedio_assembly
  ;      mov eax , [resultadoP]
        
        ;blue
        mov     eax, DWORD  [ebp-8] 
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+12] 
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]
        add     eax, ecx

        
        jmp sacar_promedio_assembly
 ;       mov eax , [resultadoP]


        inc     DWORD  [ebp-12]
        jmp     for2P
volverP:
        inc     DWORD  [ebp-8]
        jmp     for1P
salirP:
        nop
        pop     ebp
        ret


aclarar_mmx:
    push    ebp
    push    edi
    push    esi
    push    ebx 
    mov ebp, esp; for correct debugging

    movq mm0,[ebp+8]
    
    push ebp
    mov ebp, esp; for correct debugging
    
    mov edx, 2
    mov ebx,0
    ciclo:
    mov [arrayNuevo+ebx],edx
    add ebx,1
    cmp ebx, 9
    jl ciclo
    
    movq mm1 , [arrayNuevo]
    
    paddsb mm0,mm1
           

    ret
         
